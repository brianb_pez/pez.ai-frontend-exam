# Take-home Questions and Exam for Front-end developers

These questions are for take home (not in-person). Our primary goal is to understand how you think and make decisions.

## Instructions

To submit your work, you will need an account on BitBucket. We assume you already know how to use git.

You should follow the following steps before answering the questions below or beginning work on the exercises.

- [Fork this project repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) in BitBucket and give it the name `pezprogram-<firstname>-<lastname>` (using your own first/last name, of course).
- Set the visibility Access level to Private
- Give user Brian Baquiran (brianb_pez) access to the project, with Admin access permissions.

You can begin working on the problems locally.

- git clone the project to your local machine
- Be sure to commit and push often.
- For questions that require coding, you are strongly encouraged to provide unit tests and documentation for your work.

## Questions

To answer the questions, simply edit this Markdown file, placing your answers after the respective questions. You may add directories and files to the repository to illustrate your point. 

1. Based on your experience, discuss the merits of different unit testing frameworks in Javascript. Focus on specific use cases that were either effective or not effective using a given framework. If you have not used any unit testing frameworks, explain why. Do not regurgitate points online.
1. What strategies have you used for headless UI testing? Discuss pros and cons. What do you think is an ideal approach that balances ease of use with maintainability (as the GUI changes)?
1. What is asynchronous programming? How is it used in GUI programming? Give an example of where you've used it and what it solved. Provide a code sample.
1. What's the difference between JS for node.js vs browser? How can a Javascript package be used in both environments?
1. In CSS, what is the difference between ".my-component" and "#my-component"? When is appropriate to use . versus #?
1. Every framework is opinionated to a certain degree. The opinions make some tasks easy to perform and others very hard to perform. Describe a situation where your framework of choice gets in the way because of its opinions. How did you get around it? Is the framework opinion a reasonable opinion?
1. Write example code using GUI toolkit of your choice (bootstrap, angular, react) to fetch data from a REST API and create a reusable component to display a gauge. Do not use an external graphing library. What sort of interface/function signature did you provide and why? What options did you provide to configure the gauge and why? When do you think a gauge is appropriate to use?

These questions are related to [arbitrage](https://github.com/zatonovo/arbitrage), a Javascript library created by our founder. We use this library to create data visualization and analytics dashboards. Take some time to study the library.

1. What makes arbitrage different from other JS libraries? What type of programming does it promote?
1. Choose two functions in arbitrage and write unit tests for them. If unit tests already exist, identify some edge cases to test. Do you think the edge cases are realistic? Why or why not?
1. Many data APIs return table data as lists of JSON records, e.g. `[ { "ka":"va1", "kb":"vb1", "kc":"vc1" }, { "ka":"va2", "kb":"vb2", "kc":"vc2" }, ... ]`. Use arbitrage.js to transform this representation into an arbitrage data frame, which is a column-major format. Why do you think arbitrage uses a column-major data structure instead of row-major like typical JSON records?

Again, for questions that require code to answer, simply add your sample code to the repository. 

Inform us via email at `jobs@pez.ai` when you are done, with a link to your repository.

_Good luck!_
